<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "followers".
 *
 * @property int $id
 * @property string $follower_id
 * @property string $leader_id
 * @property string $created_at
 * @property string $updated_at
 */
class Followers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'followers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['follower_id', 'leader_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'follower_id' => 'Follower ID',
            'leader_id' => 'Leader ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
