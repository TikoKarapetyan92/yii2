
$(document).ready(function () {


    // Toggling the nav menu
    $('.menu-toggle').click(function () {
        $('.menu-background, .menu-card, .menu-content').toggleClass('active');
    });
    // Toggle the nav-search
    $('.search-icon').click(function () {
        $('.nav-search, .sign-div').toggleClass('active');
    });
});

$(function () {
    $('#modelButton').click(function () {
        $('.modal').modal('show')
            .find('#modelContent')
            .load($(this).attr('value'));
    });
});

// The rel attribute is the userID you would want to follow

$('.followButton').on('click', function (e) {
    e.preventDefault();
    $button = $(this);
    let id = $(this).attr('id');

    let data = {
        ID: id,
        _csrf: yii.getCsrfToken()
    };

    if ($button.hasClass('following')) {

        //$.ajax(); Do Unfollow

        let url = '/user/unfollow';
        sendAjax(url, data) ;

        $button.removeClass('following');
        $button.removeClass('unfollow');
        $button.text('Follow');
    } else {

        // $.ajax(); Do Follow
            let url = '/user/follow';
        sendAjax(url, data) ;



        $button.addClass('following');
        $button.text('Following');
    }
});

$('.followButton').hover(function () {
    $button = $(this);
    if ($button.hasClass('following')) {
        $button.addClass('unfollow');
        $button.text('Unfollow');
    }
}, function () {
    if ($button.hasClass('following')) {
        $button.removeClass('unfollow');
        $button.text('Following');
    }
});

$('.option-album').on('change', function () {
    let id = $(this).attr('id');
    let selectVal = $(this).val();
    let url = $(this).find(':selected').data('url');

    if (selectVal === '1') {
        openModal(
            url,
            function (){
                $('#albums-parent_id').val(id);
            }
        );
        console.log(url);
    } else if (selectVal === '2') {
        
        openModal(
            url,
            function (){
            $('#images-album_id').val(id);
                 }
            );

        console.log(url);
    } else if (selectVal === '3') {

        let url = '/delete/album';
        let data = {
            ID: id,
            _csrf: yii.getCsrfToken()
        };
        // console.log(url);
        if (confirm('Are you sure you want to delete this?')) {
            sendAjax(url, data);
        }
    }else if(selectVal === 'delete'){
        let url = '/delete/image';
        let data = {
            ID: id,
            _csrf: yii.getCsrfToken()
        };
        // console.log(url);
        if (confirm('Are you sure you want to delete this?')) {
            sendAjax(url, data);
        }
    }

});

$(function (){
    var star = '.star',
        selected = '.selected';

    $(star).on('click', function(){
        $(selected).each(function(){
            $(this).removeClass('selected');
        });
        $(this).addClass('selected');
        let val = $('.selected').val();
        let id = $('.option-album').val();

        let url = '/image/rating';
        let data = {
            imageId: id,
            rating: val,
            _csrf: yii.getCsrfToken()
        };
            sendAjax(url, data);
    });

});

function sendAjax(url, data) {
    $.ajax({
        url: yii.getBaseCurrentUrl() + url,
        type: 'post',
        data: data,
        success: function (data) {
            console.log(data);
        },
        error: function (err) {
            console.log(err);
        }
    });
}

function openModal(url,selector){
    $('.modal').modal('show')
        .find('#modelContent')
        .load(url,function () {
            selector.call();
        });
}


