<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\widgets\Menu;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>


<div class="nav-bar">
    <div class="horz-center nav-container">
        <div class="floated logo">Album
        </div>
        <?php
        echo Menu::widget([
            'items' => [
                ['label' => 'Users', 'options' => ['class' => 'rela-inline'], 'url' => ['user/index']],
                ['label' => 'My Profile', 'options' => ['class' => 'rela-inline'], 'url' => ['profile/'.Yii::$app->user->id ]],
            ],
            'options' => [
                'class' => 'floated nav-div nav-links',
            ],
        ]);
        ?>
        <ul class="floated nav-div">
            <li class="rela-inline menu-toggle">•••</li>
        </ul>
        <div class="floated right nav-div search-container">
            <div class="rela-inline icon search-icon"></div>
            <input type="text" placeholder="Search" class="rela-inline nav-search"/>
        </div>


        <ul class="floated right nav-div sign-div">
            <?php
            if (Yii::$app->user->isGuest) {
                echo '<li class="rela-inline">' . Html::a('Sign-Up', ['/site/signup'], ['class' => 'profile-link']) . '</li>';
                echo '<li class="rela-inline">' . Html::a('Sign-In', ['/site/login'], ['class' => 'profile-link']) . '</li>';
            } else {
                echo '<li class="rela-inline">'
                    . Html::beginForm(['/site/logout'], 'post')
                    . Html::submitButton(
                        'Logout (' . Yii::$app->user->identity->username . ')',
                        ['class' => 'btn btn-link logout']
                    )
                    . Html::endForm()
                    . '</li>';
            }
            ?>
        </ul>
        <div class="menu-card">
            <div class="abs-center menu-background">

            </div>
            <div class="menu-content">
                <?php
                echo Menu::widget([
                    'items' => [
                        ['label' => 'Users', 'options' => ['class' => 'rela-block'], 'url' => ['users']],
                        ['label' => 'My Profile', 'options' => ['class' => 'rela-block'], 'url' => ['profile/'.Yii::$app->user->id ]],
                    ],
                    'options' => [
                        'class' => 'menu-links',
                    ],
                ]);
                ?>
                <ul class="menu-links sign-links">
                    <?php
                    if (Yii::$app->user->isGuest) {
                        echo '<li class="rela-block">' . Html::a('Sign-Up', ['/site/signup'], ['class' => 'profile-link']) . '</li>';
                        echo '<li class="rela-block">' . Html::a('Sign-In', ['/site/login'], ['class' => 'profile-link']) . '</li>';
                    } else {
                        echo '<li class="rela-block">'
                            . Html::beginForm(['/site/logout'], 'post')
                            . Html::submitButton(
                                'Logout (' . Yii::$app->user->identity->username . ')',
                                ['class' => 'btn btn-link logout']
                            )
                            . Html::endForm()
                            . '</li>';
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <?= Alert::widget() ?>
    <?= $content ?>
</div>
<!--</div>-->
<?php
Modal::begin([
    'header' => '<h4>Destination</h4>',
    'id' => 'model',
    'size' => 'model-lg',
]);

echo "<div id='modelContent'></div>";

Modal::end();

?>
<footer class="rela-block footer">
    <p>FOOTER</p>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
