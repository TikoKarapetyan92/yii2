<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
<?= $form->field($model, 'user_id')->hiddenInput(['value' => Yii::$app->user->id])->label(false); ?>
<?= $form->field($model, 'parent_id')->hiddenInput(['value' => null])->label(false); ?>

<?= $form->field($model, 'name')->textInput(['maxlength' => true]); ?>
<?= $form->field($model, 'description')->textarea(['rows' => 6]); ?>
<?= $form->field($model, 'cover_image')->fileInput(); ?>

<div class="form-group">
    <?= Html::submitButton('Create', ['class' => 'btn btn-success']); ?>
</div>

<?php ActiveForm::end(); ?>
