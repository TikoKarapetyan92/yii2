<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model \app\models\Albums */


?>
<div class="box box-info">
    <div class="box-body">
        <div class="article-create">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>

        </div>
    </div>
</div>