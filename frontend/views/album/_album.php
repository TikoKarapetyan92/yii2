<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
    <h2>Album</h2>
    <hr>
    <div class="row">
        <?php foreach ($albums as $album): ?>

            <div class="col-sm-4">
                <div class="gal-detail thumb">
                    <?php if ($album->user->id === Yii::$app->user->id): ?>
                        <select class="mbp-4 option-album pull-right" name="" id="<?= $album->id; ?>">
                            <option>Choose an option</option>
                            <option value="1" data-url="<?= \yii\helpers\Url::to(['create/album']); ?>">Add album
                            </option>
                            <option value="2" data-url="<?= \yii\helpers\Url::to(['create/image']); ?>">
                                Add image
                            <option value="3">Delete</option>
                        </select>
                    <?php endif; ?>
                    <?= Html::a(Html::img('@web/uploads/albums/' . $album->cover_image, ['class' => 'thumb-img']), ['album/' . $album->slug], [
                        'class' => 'image-popup',
                    ]); ?>
                    <h4 class="text-center"><?= $album->name; ?></h4>
                    <div class="ga-border">
                        <div class="txt-center">
                            <div class="rating">
                                <input id="star5" name="star" type="radio" value="5" class="radio-btn star hide"/>
                                <label for="star5">☆</label>
                                <input id="star4" name="star" type="radio" value="4" class="radio-btn star hide"/>
                                <label for="star4">☆</label>
                                <input id="star3" name="star" type="radio" value="3" class="radio-btn star hide"/>
                                <label for="star3">☆</label>
                                <input id="star2" name="star" type="radio" value="2" class="radio-btn star hide"/>
                                <label for="star2">☆</label>
                                <input id="star1" name="star" type="radio" value="1" class="radio-btn star hide"/>
                                <label for="star1">☆</label>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                    <p class="text-muted text-center">
                        <small><?= $album->description; ?></small>
                    </p>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php if (!empty($images)): ?>
    <?= $this->render('//image/_image', [
        'images' => $images,
    ]) ?>
<?php endif; ?>