<?php

use yii\helpers\Html;
use yii\helpers\Url;

//var_dump($images);die;
?>
<h2>Image</h2>
<hr>
<div class="row">
    <!--<div class="rela-block content" >-->

    <?php foreach ($images as $image): ?>
        <div id="comment" data-url="<?php echo Url::base(true); ?>"></div>

        <div class="col-sm-4">
            <div class="gal-detail thumb">
                <?php if ($image->album->user_id === Yii::$app->user->id): ?>
                    <select class="mbp-4 option-album pull-right" name="" id="<?= $image->id; ?>">
                        <option>Choose an option</option>
                        <option value="delete">Delete</option>
                    </select>
                <?php endif; ?>
                <?= Html::img('@web/uploads/albums/' . $image->image, ['class' => 'thumb-img']); ?>
                <h4 class="text-center"><?= $image->description; ?></h4>
                <div class="ga-border">
                    <div class="txt-center">
                        <div class="rating">
                            <input id="star5" name="star" type="radio" value="5" class="radio-btn star hide"/>
                            <label for="star5">☆</label>
                            <input id="star4" name="star" type="radio" value="4" class="radio-btn star hide"/>
                            <label for="star4">☆</label>
                            <input id="star3" name="star" type="radio" value="3" class="radio-btn star hide"/>
                            <label for="star3">☆</label>
                            <input id="star2" name="star" type="radio" value="2" class="radio-btn star hide"/>
                            <label for="star2">☆</label>
                            <input id="star1" name="star" type="radio" value="1" class="radio-btn star hide"/>
                            <label for="star1">☆</label>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
                <p class="text-muted text-center">
                    <small><?= $image->description; ?></small>
                </p>
            </div>
        </div>
    <?php endforeach; ?>
    <!--</div>-->
</div>