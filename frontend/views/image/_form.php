<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
<?= $form->field($model, 'album_id')->hiddenInput(['value' => ''])->label(false); ?>

<?= $form->field($model, 'image')->fileInput(); ?>
<?= $form->field($model, 'description')->textarea(['rows' => 6]); ?>

<div class="form-group">
    <?= Html::submitButton('Create', ['class' => 'btn btn-success']); ?>
</div>

<?php ActiveForm::end(); ?>
