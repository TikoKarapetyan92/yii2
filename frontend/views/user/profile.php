<?php

use yii\helpers\Html;

?>
<div class="rela-block container">
    <div class="rela-block profile-card">
        <div class="profile-pic" id="profile_pic"></div>
        <div class="rela-block profile-name-container">
            <div class="rela-block user-name"><?= $user->username; ?></div>
            <?php if ($user->id !== Yii::$app->user->id): ?>
                <div class="rela-block user-follow">
                    <button class="btn-follow followButton <?= empty($follower) ? '' : 'following' ?>"
                            id="<?= $user->id; ?>" rel="6">Follow
                    </button>
                </div>
            <?php endif; ?>
        </div>
        <div class="rela-block profile-card-stats">
            <div class="floated profile-stat followers"><?= $followers ?><br></div>
            <div class="floated profile-stat"><br></div>
            <div class="floated profile-stat follow"><?= $following ?><br></div>
        </div>
    </div>

    <?php if ($user->id === Yii::$app->user->id): ?>
        <?= Html::button('Add album', ['id' => 'modelButton', 'value' => \yii\helpers\Url::to(['create/album']), 'class' => 'rela-inline button more-images']) ?>
    <?php endif; ?>

    <?= $this->render('//album/_album', [
        'albums' => $albums,
    ]) ?>
</div>
