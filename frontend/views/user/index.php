<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;

?>

<div class="card">
    <div class="card-header">
        <h2>All users</h2>
    </div>
    <?php if (!empty($users)): ?>
        <?php foreach ($users as $user): ?>
            <div class="gaadiex-list-item">
                <?= Html::img('@web/assets/images/user-icon.png', ['class' => 'gaadiex-list-item-img', 'alt' => 'user']) ?>
                <div class="gaadiex-list-item-text">
                    <h3>
                        <?= Html::a($user->username, ['profile/'.$user->id], ['class' => 'profile-link']) ?>
                    </h3>
<!--                    <h4>f</h4>-->
                   <div style="height: 35px;"></div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <h3>data not found</h3>
    <?php endif; ?>

    <?= LinkPager::widget(['pagination' => $pagination]) ?>

</div>

