<?php
/**
 * Created by PhpStorm.
 * User: Sony
 * Date: 31.01.2019
 * Time: 15:50
 */

namespace frontend\controllers;

use app\models\Albums;
use app\models\Followers;
use common\models\User;
use Yii;
use yii\data\Pagination;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;


class UserController extends Controller
{
    public function behaviors()
    {

        $behaviors = [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
//        $behaviors = [];/
        return $behaviors;
    }

    public function actionIndex()
    {
        $query = User::find()
            ->where(['!=', 'id', Yii::$app->user->id]);

        $pagination = new Pagination([
            'defaultPageSize' => 10,
            'totalCount' => $query->count(),
        ]);

        $users = $query->orderBy('username')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('index', [
            'users' => $users,
            'pagination' => $pagination,

        ]);
    }

    public function actionFollow()
    {
        $model = new Followers();
        $model->follower_id = Yii::$app->user->id;
        $model->leader_id = Yii::$app->request->post('ID');
        if ($model->save()) {
            Yii::$app->session->setFlash('success', 'Successfully followed the user.');

            return $this->redirect(['profile', 'id' => $model->leader_id]);
        }
    }

    public function actionUnfollow()
    {
        $follower_id = Yii::$app->user->id;
        $leader_id = Yii::$app->request->post('ID');
        $model = Followers::find()
            ->where(['follower_id' => $follower_id, 'leader_id' => $leader_id])
            ->one();

        if ($model->delete()) {
            Yii::$app->session->setFlash('success', 'Successfully unfollowed the user.');

            return $this->redirect(['profile', 'id' => $model->leader_id]);
        }
    }

    public function actionProfile($id)
    {
        $user = User::find()
            ->where(['id' => $id])
            ->one();

        $follower_id = Yii::$app->user->id;

        $follower = Followers::find()
            ->where(['follower_id' => $follower_id, 'leader_id' => $user->id])
            ->one();

        $following = Followers::find()
            ->where(['leader_id' => $user->id])
            ->count();

        $followers = Followers::find()
            ->where(['follower_id' => $user->id])
            ->count();

        $query = Albums::find()
            ->where(['user_id' => $id, 'parent_id' => null]);

        $pagination = new Pagination([
            'defaultPageSize' => 10,
            'totalCount' => $query->count(),
        ]);

        $albums = $query->orderBy('created_at')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        return $this->render('profile', [
            'user' => $user,
            'albums' => $albums,
            'follower' => $follower,
            'following' => $following,
            'followers' => $followers
        ]);
    }

}