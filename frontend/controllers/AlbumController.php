<?php

namespace frontend\controllers;


use app\models\Albums;
use app\models\Images;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\BaseFileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * AlbumController implements the CRUD actions for Albums model.
 */
class AlbumController extends Controller
{

    public function behaviors()
    {

        $behaviors = [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
//        $behaviors = [];/
        return $behaviors;
    }
    /**
     * Lists all Albums models.
     * @return mixed
     */
    public function actionIndex($slug)
    {
        $query = Albums::findOne(['slug' => $slug]);
        $images = Images::find()->where(['album_id' => $query->id])->all();
        $albums = Albums::find()->where(['parent_id' => $query->id])->with('images')->all();
//        echo"<pre>";
//        var_dump($query);die;
        return $this->render('index', [
            'albums' => $albums,
            'images' => $images,
        ]);
    }

    /**
     * Displays a single Albums model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($slug)
    {
        $query = Albums::findOne(['slug' => $slug]);
        $images = Images::find()
            ->where(['album_id' => $query->id])
            ->all();
        $albums = Albums::find()
            ->where(['id' => $query->id])
            ->all();

        return $this->render('view', [
            'albums' => $albums,
            'images' => $images,
        ]);
    }

    /**
     * Creates a new Albums model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new Albums();
        $upload = UploadedFile::getInstance($model, 'cover_image');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (!empty($upload)) {
                $this->fileUpload($model->id);
            }
            Yii::$app->session->setFlash('success', 'Successfully.');
            return $this->redirect(['view', 'slug' => $model->slug]);
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }


    public function fileUpload($id)
    {

        $path = Yii::getAlias("@frontend/web/uploads/albums");

        BaseFileHelper::createDirectory($path);

        $model = $this->findModel($id);
        $file = UploadedFile::getInstance($model, 'cover_image');
        $symbols = '0123456789abcdefghijklmnopqrstuvwxyz';
        $filename = substr(str_shuffle($symbols), 0, 16);

        $name = $filename . '.' . $file->extension;
        $file->saveAs($path . DIRECTORY_SEPARATOR . $name);

        $model->cover_image = $name;
        $model->save();
    }


    /**
     * Deletes an existing Albums model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete()
    {
        $id = Yii::$app->request->post('ID');
        $img = Albums::findOne($id);
        $author_id = $img->user_id;
        if (Yii::$app->request->isAjax && ($author_id === Yii::$app->user->id)) {
            $this->layout = false;
            $id = Yii::$app->request->post('ID');
            $status = $this->findModel($id)->delete();
            if (!$status) {
                Yii::$app->session->setFlash('error', 'Deleted.');
            }
            Yii::$app->session->setFlash('success', 'Successfully.');
            $this->redirect(['/user/profile', 'id' => Yii::$app->user->id]);
        }
    }

    /**
     * Finds the Albums model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Albums the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Albums::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


}
