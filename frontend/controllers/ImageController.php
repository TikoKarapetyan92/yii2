<?php

namespace frontend\controllers;


use app\models\Albums;
use app\models\Images;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\BaseFileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * ImageController implements the CRUD actions for Images model.
 */
class ImageController extends Controller
{

    public function behaviors()
    {

        $behaviors = [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
//        $behaviors = [];/
        return $behaviors;
    }

    /**
     * Displays a single Images model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($slug)
    {

        $query = Albums::findOne(['slug'=>$slug]);
        $images = Images::find()
            ->joinWith('album')
            ->where(['album_id'=> $query->id])
            ->all();
//        echo"<pre>";
//        var_dump($albums);die;
        return $this->render('_image', [
            'images' => $images,
        ]);
    }


    /**
     * Creates a new Images model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new Images();

        $upload = UploadedFile::getInstance($model,'image');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if(!empty($upload)) {
                $this->fileUpload($model->id);
            }

            Yii::$app->session->setFlash('success', 'Successfully.');
            return $this->redirect(['view', 'slug' => $model->album->slug]);
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function fileUpload($id){

        $path = Yii::getAlias("@frontend/web/uploads/albums");

        BaseFileHelper::createDirectory($path);

        $model = $this->findModel($id);
        $file = UploadedFile::getInstance($model,'image');
        $symbols = '0123456789abcdefghijklmnopqrstuvwxyz';
        $filename = substr(str_shuffle($symbols), 0, 16);

        $name = $filename.'.'.$file->extension;
        $file->saveAs($path .DIRECTORY_SEPARATOR .$name);

        $model->image = $name;
        $model->save();
    }

    public function actionRating()
    {
        echo 555;
    }


    /**
     * Deletes an existing Images model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete()
    {
        $id = Yii::$app->request->post('ID');
        $img = Images::findOne($id);
        $author_id = $img->album->user_id;
        if (Yii::$app->request->isAjax && ($author_id === Yii::$app->user->id)) {
            $this->layout = false;
            $id = Yii::$app->request->post('ID');
            $status = $this->findModel($id)->delete();
            if (!$status) {
                Yii::$app->session->setFlash('error', 'Deleted.');
            }
            Yii::$app->session->setFlash('success', 'Successfully.');
            $this->redirect(['/user/profile', 'id' => Yii::$app->user->id]);
        }

    }

    /**
     *
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Images the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Images::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


}
