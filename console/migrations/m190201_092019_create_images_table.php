<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%images}}`.
 */
class m190201_092019_create_images_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%images}}', [
            'id' => $this->primaryKey(),
            'album_id' => $this->integer()->notNull(),
            'image' => $this->string()->notNull(),
            'description' => $this->string()->null(),

            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),

        ]);
        $this->addForeignKey(
            'album_id',
            'images',
            'album_id',
            'albums',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%images}}');

        $this->dropForeignKey(
            'album_id',
            'albums'
        );

    }
}
