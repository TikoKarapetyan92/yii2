<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%albums}}`.
 */
class m190201_091724_create_albums_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%albums}}', [
            'id' => $this->primaryKey(),
            'user_id' =>$this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'slug' =>$this->string()->notNull(),
            'description' => $this->text()->null(),
            'cover_image' => $this->string()->null(),
            'parent_id' =>$this->integer()->null(),

            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
        ]);

        $this->addForeignKey(
            'user_id',
            'albums',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%albums}}');

        $this->dropForeignKey(
            'user_id',
            'user'
        );
    }

}
