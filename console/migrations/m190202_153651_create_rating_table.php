<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%rating}}`.
 */
class m190202_153651_create_rating_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%rating}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'image_id' => $this->integer()->notNull(),
            'rating' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey(
            'image_id',
            'rating',
            'image_id',
            'images',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%rating}}');

        $this->dropForeignKey(
            'image_id',
            'images'
        );
    }
}
