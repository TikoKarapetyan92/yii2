<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%followers}}`.
 */
class m190202_155612_create_followers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%followers}}', [
            'id' => $this->primaryKey(),
            'follower_id' =>$this->integer()->unsigned(),
            'leader_id' =>$this->integer()->unsigned(),

            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%followers}}');
    }
}
